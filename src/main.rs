mod unit_test;

use std::io;

fn main() {
    println!("Entrez un nombre décimal à convertir en chiffres romains :");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Erreur lors de la lecture de l'entrée");
    let input: u32 = input.trim().parse().expect("Entrée invalide");
    let roman_numeral = decimal_to_roman(input);
    println!("Le chiffre romain équivalent à {} est : {}", input, roman_numeral);
}


fn decimal_to_roman(mut number: u32) -> String {
    let roman_digits = vec![
        (1000, "M"), (900, "CM"), (500, "D"), (400, "CD"),
        (100, "C"), (90, "XC"), (50, "L"), (40, "XL"),
        (10, "X"), (9, "IX"), (5, "V"), (4, "IV"), (1, "I")
    ];
    let mut result = String::new();
    for &(value, symbol) in roman_digits.iter() {
        while number >= value {
            result.push_str(symbol);
            number -= value;
        }
    }
    result
}
