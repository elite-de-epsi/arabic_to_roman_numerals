#[cfg(test)]
mod tests {
    use crate::decimal_to_roman;

    #[test]
    fn roman_to_decimal_test_1() {
        assert_eq!(decimal_to_roman(1), "I");
    }

    #[test]
    fn roman_to_decimal_test_2() {
        assert_eq!(decimal_to_roman(2), "II");
    }

    #[test]
    fn roman_to_decimal_test_3() {
        assert_eq!(decimal_to_roman(3), "III");
    }

    #[test]
    fn roman_to_decimal_test_4() {
        assert_eq!(decimal_to_roman(4), "IV");
    }

    #[test]
    fn roman_to_decimal_test_5() {
        assert_eq!(decimal_to_roman(5), "V");
    }

    #[test]
    fn roman_to_decimal_test_6() {
        assert_eq!(decimal_to_roman(6), "VI");
    }

    #[test]
    fn roman_to_decimal_test_7() {
        assert_eq!(decimal_to_roman(7), "VII");
    }
    
    #[test]
    fn roman_to_decimal_test_8() {
        assert_eq!(decimal_to_roman(8), "VIII");
    }

    #[test]
    fn roman_to_decimal_test_9() {
        assert_eq!(decimal_to_roman(9), "IX");
    }

    #[test]
    fn roman_to_decimal_test_10() {
        assert_eq!(decimal_to_roman(10), "X");
    }

    #[test]
    fn roman_to_decimal_test_11() {
        assert_eq!(decimal_to_roman(11), "XI");
    }
    
    #[test]
    fn roman_to_decimal_test_12() {
        assert_eq!(decimal_to_roman(12), "XII");
    }
    
    #[test]
    fn roman_to_decimal_test_13() {
        assert_eq!(decimal_to_roman(13), "XIII");
    }
    
    #[test]
    fn roman_to_decimal_test_14() {
        assert_eq!(decimal_to_roman(14), "XIV");
    }
    
    #[test]
    fn roman_to_decimal_test_15() {
        assert_eq!(decimal_to_roman(15), "XV");
    }

    #[test]
    fn roman_to_decimal_test_16() {
        assert_eq!(decimal_to_roman(16), "XVI");
    }
    
    #[test]
    fn roman_to_decimal_test_17() {
        assert_eq!(decimal_to_roman(17), "XVII");
    }

    #[test]
    fn roman_to_decimal_test_18() {
        assert_eq!(decimal_to_roman(18), "XVIII");
    }

    #[test]
    fn roman_to_decimal_test_19() {
        assert_eq!(decimal_to_roman(19), "XIX");
    }

    #[test]
    fn roman_to_decimal_test_20() {
        assert_eq!(decimal_to_roman(20), "XX");
    }

    #[test]
    fn roman_to_decimal_test_21() {
        assert_eq!(decimal_to_roman(21), "XXI");
    }

    #[test]
    fn roman_to_decimal_test_22() {
        assert_eq!(decimal_to_roman(22), "XXII");
    }
    #[test]
    fn roman_to_decimal_test_23() {
        assert_eq!(decimal_to_roman(23), "XXIII");
    }
    #[test]
    fn roman_to_decimal_test_24() {
        assert_eq!(decimal_to_roman(24), "XXIV");
    }
    #[test]
    fn roman_to_decimal_test_25() {
        assert_eq!(decimal_to_roman(25), "XXV");
    }
    #[test]
    fn roman_to_decimal_test_26() {
        assert_eq!(decimal_to_roman(26), "XXVI");
    }
    #[test]
    fn roman_to_decimal_test_27() {
        assert_eq!(decimal_to_roman(27), "XXVII");
    }
    #[test]
    fn roman_to_decimal_test_28() {
        assert_eq!(decimal_to_roman(28), "XXVIII");
    }
    #[test]
    fn roman_to_decimal_test_29() {
        assert_eq!(decimal_to_roman(29), "XXIX");
    }
    #[test]
    fn roman_to_decimal_test_30() {
        assert_eq!(decimal_to_roman(30), "XXIX");
    }
}


